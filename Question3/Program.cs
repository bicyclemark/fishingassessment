﻿using System;

namespace Question3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] case1 = new int[] { 1, 100, -200, 400, -22 };
            int[] case2 = new int[] { -1, 50, 22, -400, 1, 22, 38 };

            Console.WriteLine("Given an arbitrary length array of ints find the index and sum of the index with the max sum of remaining items");
            Question q1 = new Question(case1);
            q1.SolveAndPrint("Case 1");

            Question q2 = new Question(case2);
            q2.SolveAndPrint("Case 2");
        }
    }
}
