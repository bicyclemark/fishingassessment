﻿using System;
using Question;

namespace Question3
{
    public class Question : AbstractQuestion
    {
        private int[] items;

        public Question() : base()
        {
        }

        public Question(int[] vector = null) : base()
        {
            
            if (vector != null)
            {
                AssignParameters(vector);
            }
        }

        public void AssignParameters(int[] vector)
        {
            items = vector;
            for (int i = 0; i < vector.Length; i++)
            {
                Parameters.Add($"p{i}", vector[i]);
            }
        }

        public override void Solve()
        {
            Solution.Clear();
            int max_ndx = -1;
            int max_sum = int.MinValue;
            for (int i = 0; i < items.Length; i++)
            {
                int current_sum = 0;
                for (int j = i; j < items.Length; j++)
                {
                    current_sum += items[j];
                }
                if (current_sum >= max_sum)
                {
                    max_sum = current_sum;
                    max_ndx = i;
                }
            }
            Solution.Add(nameof(max_ndx), max_ndx);
            Solution.Add(nameof(max_sum), max_sum);

        }
    }
}
