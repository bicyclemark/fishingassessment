﻿using System;
using System.Collections.Generic;
/*

*/
namespace Question2
{
    class Program
    {  
        static void Main(string[] args)
        {
            uint case1 = 32768;
            uint case2 = 10;
            Console.WriteLine(  "You will be given a 32 - bit unsigned integer.You are required to give as output a integer you get by "+
                                "reversing bits in its binary representation.");
            Question q1 = new Question(case1);
            q1.SolveAndPrint("Case 1");

            Question q2 = new Question(case2);
            q2.SolveAndPrint("Case 2");
        }
    }
}
