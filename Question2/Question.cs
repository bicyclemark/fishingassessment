﻿using System;
using Question;

namespace Question2
{
    public class Question : AbstractQuestion
    {
        public Question() : base()
        {

        }

        public Question(uint input) : base()
        {
            Parameters.Add(nameof(input), input);
            Solution.Clear();
        }

        public  override void Solve()
        {
            uint input = (uint)Parameters["input"];
            uint reverse_num = 0;
            int count = sizeof(uint) * 8 - 1;
            input >>= 1;
            while (input != 0)
            {
                reverse_num <<= 1;
                reverse_num |= input & 1;
                input >>= 1;
                count--;
            }
            reverse_num <<= count;
            Solution.Add("output", reverse_num);
        }
    }
}
