using System.Collections.Generic;
using NUnit.Framework;

namespace Question1.UnitTest
{
    public class Tests
    {
       
        static int[] testCase1 = new int[] { 3, 1, 2, 3, 6 };
        static int[] testCase2 = new int[] { 5, 2, -1, 3, 1, 4, 9 };
       
        [TestCase(1)]
        [TestCase(2)]

        public void TestSolveQuestion1(int testCase)
        {
            int[] lstVector = null;
            switch(testCase)
            {
                case (1):
                    {
                        lstVector = testCase1;
                        break;
                    }
                case (2):
                    {
                        lstVector = testCase2;
                        break;
                    }
            }
            Assert.IsNotNull(testCase);
            Question q = new Question(lstVector); 
            q.Solve();
            Assert.AreEqual(q.Solution["output"], lstVector[lstVector.Length - 1]);

        }
    }
}