using NUnit.Framework;

namespace Question3.UnitTest
{
    public class Tests
    {
        TestData[] testData;
        private struct TestData
        {
            public TestData(int[] vector, int max_ndx)
            {
                items = vector;
                expected_index = max_ndx;
            }
            public int[] items;
            public int expected_index;
        }
        [OneTimeSetUp]
        public void Setup()
        {
            testData = new TestData[]
            { new TestData(new int[]{ 1, 100, -200, 400, -22 }, 3),
              new TestData(new int[]{ -1, 50, 22, -400, 1,22, 38}, 4)
            };

        }

        [TestCase(0,3)]
        [TestCase(1, 4)]

        public void Test(int ndx, int expectedNdx)
        {
            TestData td = testData[ndx];
            Question q = new Question(td.items);
            q.Solve();
            Assert.IsNotNull(q.Solution);
            
        }
    }
}