﻿using System.Collections.Generic;
using NUnit.Framework;

namespace Question2.UnitTest
{
    public class Tests
    {
        struct TestStruct
        {
            public TestStruct(uint inp, uint outp)
            {
                input = inp;
                expectedOutput = outp;

            }
            public uint input;
            public uint expectedOutput;

        }
        private static TestStruct[] testData;
        [OneTimeSetUp]
        public void SetTestData()
        {
           testData = new TestStruct[] {new TestStruct( 32768, 65536),
                                        new TestStruct( 10,1342177280) };

        }
        
        [TestCase(0)]
        [TestCase(1)]
        public void TestSolveCase1(int ndx)
        {
            Question q = new Question(testData[ndx].input);
            q.Solve();
            Assert.AreEqual(q.Solution["output"], testData[ndx].expectedOutput);
        }
    }
}
