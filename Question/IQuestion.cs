﻿using System.Collections.Generic;

namespace Question
{
    public interface IQuestion
    {
        Dictionary<string, object> Parameters   { get; set; }
        Dictionary<string, object> Solution     { get; set; }

        void Initialize(Dictionary<string, object> parms);
        void PrintParameters(string heading=null);
        void PrintSolution(string heading = null);
        void Solve();
    }
}
