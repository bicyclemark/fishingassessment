﻿using System;
using System.Collections.Generic;

namespace Question
{
    public abstract class AbstractQuestion : IQuestion
    {
        public Dictionary<string, object> Parameters    { get;  set; }
        public Dictionary<string, object> Solution      { get;  set; }
        

        public abstract void Solve();
        public AbstractQuestion()
        {
            Parameters = new Dictionary<string, object>();
            Solution = new Dictionary<string, object>();
        }

        public void Initialize(Dictionary<string, object> parms)
		{
            Parameters = parms;
            Solution.Clear();
		}

        public void PrintParameters(string heading = null)
		{
            print(Parameters, heading);
		}

        public void PrintSolution(string heading = null)
		{
            print(Solution, heading);
        }

        private void print(Dictionary<string,object> dict, string heading)
		{
            if (heading != null)
                Console.WriteLine(heading);
            foreach (var kvp in dict)
			{
                Console.WriteLine($"{kvp.Key} = {kvp.Value}");
			}
		}

        public void SolveAndPrint( string name)
        {
            Console.WriteLine();
            Console.WriteLine("==================================================");
            Console.WriteLine(name);
            Console.WriteLine("==================================================");
            Console.WriteLine("Parameters:");
            PrintParameters();
            Solve();
            Console.WriteLine("==================================================");
            Console.WriteLine("Solution:");
            PrintSolution();
            Console.WriteLine("==================================================");
            Console.WriteLine();
        }
    }
}
