﻿using System;
using Question;

namespace Question1
{
    public class Question : AbstractQuestion
    {
        public Question( int[] vector = null) :base()
        {
            if (vector != null)
            {
                AssignParameters(vector);
            }
        }

        public void AssignParameters(int[] vector)
        {
            Parameters.Add("blockCount", vector[0]);
            for (int i = 1; i <= vector[0]; i++ )
            {
                Parameters.Add($"p{i - 1}", vector[i]);
            }
            Parameters.Add("expectedSum", vector[vector.Length - 1]);
        }

        public override void Solve()
        {
            Solution.Add("output", 0);
            long sum = 0;
            int upper = (int)Parameters["blockCount"];
            for (int i = 1; i <= upper; i++)
            {
                sum += (int)Parameters[$"p{i - 1}"];
            }
            Solution["output"] = sum;
        }
    }
}
