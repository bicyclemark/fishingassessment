﻿using System;
using System.Collections.Generic;
/*

*/
namespace Question1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] testCase1 = new int[] { 3, 1, 2, 3, 6 };
            int[] testCase2 = new int[] { 5, 2, -1, 3, 1, 4, 9 };

            Console.WriteLine(  "Given a list of integers, find the maximum possible sum you can get from any contiguous segment"+
                                "from the list.List size will be given in the first line of the input.List elements will be given one per line.");

            Question q1 = new Question(testCase1);
            q1.SolveAndPrint("Case 1");
           
            Question q2 = new Question(testCase2);
            q2.SolveAndPrint("Case 2");  
        }
    }
}
